#include <userlib.h>
#include <userASMlib.h>
#include <shellModule.h>

#define STDIN 1

extern uint8_t bss;
extern uint8_t endOfBinary;

void * memset(void * destination, int32_t c, uint64_t length);
void clearBSS(void * bssAddress, uint64_t bssSize);


int main() {


	clearBSS(&bss, &endOfBinary - &bss);

	setLocalTime();


	while(TRUE){
		printf("$>");
		scanf();	
		shell();
	}

	
	return 0;

}


void clearBSS(void * bssAddress, uint64_t bssSize)
{
	memset(bssAddress, 0, bssSize);
}


void * memset(void * destination, int32_t c, uint64_t length)
{
	uint8_t chr = (uint8_t)c;
	char * dst = (char*)destination;

	while(length--)
		dst[length] = chr;

	return destination;

}


void setLocalTime(){
	int aux;
	char hours[3];
	
	readTime(HOURS, hours);	  
	aux = atoi(hours);
	
	if(aux<3){
		aux += 24;	
	}
	
	aux -= 3;	
	aux += aux/10*6;
	setTime(HOURS, aux);
}