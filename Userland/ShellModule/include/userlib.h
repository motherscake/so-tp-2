
#ifndef USER_LIB_
#define USER_LIB_
#include <stdint.h>

#define HOURS 0x04
#define MINUTES 0x02
#define SECONDS 0x00

#define TRUE 1
#define FALSE 0
#define NULL (void*)0

typedef enum {RUNNING, READY, BLOCK} state;

typedef struct
{
	int pid;
	state state;
	char * name;
	int memorySpace;
	int isForeground;
	
} ProcessCharcs;

typedef struct
{
	int semid;
	int counter;
    int blockProcess;
    /*
    int firstId;
    int lastId;
    SemNode * first;
    SemNode * last;
    */
	
} SemaphCharcs;


void printChar(char c);

void printf (const char *format, ...);

char getChar();

int readline(char * buffer);

void itoa(int num, char arr[]);

int atoi(char * arr);

int strlen(char * str);

int strcmp(char* str1, char* str2);

void printBase(uint64_t value, uint32_t base);

void printString(char * arg);

void printDec(uint64_t value);

void printHex(uint64_t value);

uint32_t uintToBase(uint64_t value, char * buffer, uint32_t base);

void setLocalTime();

void * alloc(int size);

void free(void * pointer);

void sleep(int seg);

#endif
