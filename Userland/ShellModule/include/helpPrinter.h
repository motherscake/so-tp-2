//helpPrinter.h
#ifndef HELPPRINTER_H_
#define HELPPRINTER_H_


void printHelpList(void);

void printHelp(void);

void printSettime(void);

void printViewtime(void);

void printSetsstime(void);

void printClear(void);

void printPs(void);

void printIpcs(void);

void printProdcons(void);

void printArrow(int length);

#endif /* HELPPRINTER_H_ */