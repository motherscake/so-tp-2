#ifndef USER_ASM_LIB
#define USER_ASM_LIB_
#include <stdint.h>

int read(int fd, char* buf, int length);

void write(int fd, char* buf, int length);

/*
 * timeRegister: HOURS, MINUTES, SECONDS
*/
void readTime(char timeRegister, char * time);

void setTime(char timeRegister, char time);

/*
 * Sets the time it takes the Screen Saver to activate	
*/
void setSSTime(int times);

void clear();

/* Memory */
void * asm_alloc(int size);

int asm_free(void * pointer);


/* Processes */

int asm_createProcess(void* entryPoint, char * name, int ppid);

void asm_deleteProcess(int pid);

ProcessCharcs * asm_listProcesses(void);

void asm_blockProcess(int pid);

void asm_unblockProcess(int pid);

void asm_blockCurrentProcess(void);

void asm_yield(void);

int asm_getCurrentPID(void);

int asm_getProcessesAmount(void);


/* Semaphores */
int * asm_shmget(int key, int memory);

int asm_shmat(void * pointer);

void asm_shmdt(int idMem);

int asm_semget(int , int );
//TODO: ver con guido
//int asm_semctl(.....);

int * asm_listSemid(int * amount);

int * asm_listShmid(int * amount);

int asm_getIpcsAmount(void);

int asm_getppid(void);

#endif
