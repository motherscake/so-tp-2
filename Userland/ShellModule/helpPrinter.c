//helpPrinter.c
#include <helpPrinter.h>
#include <userlib.h>

#define TOTAL_FUNCTIONS 8
#define NAMES_SPACE 25

typedef void (*function)();
static function functions[] = { printHelp, printSettime, printViewtime, printSetsstime,
								printClear,	printPs, printIpcs, printProdcons };



void printHelpList(void){
	printf("\nSUPPORTED COMMANDS:\n");
	printf("----------------------------------------------------------------------------\n");

	for(int i=0; i < TOTAL_FUNCTIONS; i++){
		printf("  -");
		functions[i]();
	}
	printf("\nTo send a process to background write '&' before the command.\n\n");
}

void printHelp(void){
	char * command = "help";
	printf(command);
	printArrow(strlen(command));
	printf("Displays the list of supported commands.\n");
}

void printSettime(void){
	char * command = "settime [hh]:[mm]:[ss]";
	printf(command);
	printArrow(strlen(command));
	printf("Sets the current time.\n");
}

void printViewtime(void){
	char * command = "viewtime";
	printf(command);
	printArrow(strlen(command));
	printf("Displays the current time as [hh]:[mm]:[ss].\n");
}

void printSetsstime(void){
	char * command = "setsstime [mm]";
	printf(command);
	printArrow(strlen(command));
	printf("Sets the screen saver timeout in minutes.\n");
}

void printClear(void){
	char * command = "clear";
	printf(command);
	printArrow(strlen(command));
	printf("Clears all the screen.\n");
}

void printPs(void){
	char * command = "ps";
	printf(command);
	printArrow(strlen(command));
	printf("Displays the list of processes of the system.\n");
}

void printIpcs(void){
	char * command = "ipcs";
	printf(command);
	printArrow(strlen(command));
	printf("Displays the ipc structures of the system.\n");
}

void printProdcons(void){
	char * command = "prodcons [cant_cycles]";
	printf(command);
	printArrow(strlen(command));
	printf("Shows the producer-consumer problem.\n");
}

void printArrow(int length){
	printSpace(NAMES_SPACE-length);
	printf("--->  ");
}