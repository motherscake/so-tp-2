#include <shellModule.h>
#include <userASMlib.h>
#include <userlib.h>
#include <helpPrinter.h>
#include <stdint.h>

#define SETTIME_LENGTH 7
#define SETSSTIME_LENGTH 9
#define KEY_SEM_PROD 13
#define KEY_SEM_CONS 8
#define KEY_SHM 1
#define INITSPACE 123


static char shellBuffer[SHELL_BUFF_SIZE];
static int shellPID = -1;
static int flag = FALSE;
static int repetitions = 0;
char* cmd[] = {"help", "settime", "viewtime", "setsstime","clear", "ps", "ipcs", "prodcons"};


void shell(){
	int i;
	int length;

	if(shellPID == -1){
		shellPID = asm_getCurrentPID();
	}
	if(shellBuffer[0]=='&'){
		flag = TRUE;
	}
	
	for (i=0; i<TOTAL_COMMANDS; i++){
		if(flag == FALSE){
			length = strcmp(cmd[i],shellBuffer);
		}else{
			length = strcmp(cmd[i],shellBuffer+sizeof(char));
		}
		if(length > 0){

			switch(i){
				
				case HELP:
					atomic();
					asm_createProcess((void*)executeHelp, cmd[i], shellPID);
					if(flag == FALSE){
						asm_blockProcess(shellPID);
					}
					unAtomic();
					asm_yield();
					return;

				case SETTIME:
					atomic();
					asm_createProcess((void*)executeSetTime, cmd[i], shellPID);
					if(flag == FALSE){
						asm_blockProcess(shellPID);
					}
					unAtomic();
					asm_yield();
					return;

				case VIEWTIME:
					atomic();
					asm_createProcess((void*)executeViewTime, cmd[i], shellPID);
					if(flag == FALSE){
						asm_blockProcess(shellPID);
					}
					unAtomic();
					asm_yield();
					return;

				case SETSSTIME:
					atomic();
					asm_createProcess((void*)executeSetSSTime, cmd[i], shellPID);
					if(flag == FALSE){
						asm_blockProcess(shellPID);
					}
					unAtomic();
					asm_yield();
					return;
					
				case CLEAR:
					if(shellBuffer[length]==0){
						atomic();
						asm_createProcess((void*)executeClear, cmd[i], shellPID);
						if(flag == FALSE){
							asm_blockProcess(shellPID);
						}
						unAtomic();
						asm_yield();
						return;
					}
					return;

				case PS:
					atomic();
					asm_createProcess((void*)executePs, cmd[i], shellPID);
					if(flag == FALSE){
						asm_blockProcess(shellPID);
					}
					unAtomic();
					asm_yield();
					return;

				case PRODCONS:
					atomic();
					asm_createProcess((void*)executeProdCons, cmd[i], shellPID);
					if(flag == FALSE){
						asm_blockProcess(shellPID);
					}
					unAtomic();
					asm_yield();
					return;

				case IPCS:
					atomic();
					asm_createProcess((void*)executeIpcs, cmd[i], shellPID);
					if(flag == FALSE){
						asm_blockProcess(shellPID);
					}
					unAtomic();
					asm_yield();
					return;

				default:
					printf("Invalid command\n");
					return;
			}
		}
	
	}
	printf("Invalid command\n");
	return;
}


void executeHelp(void){

	int myFlag = flag;
	int end = 4;
	int myPid = asm_getCurrentPID();

	if(flag == TRUE){
		end++;
	}
	if (shellBuffer[end]!=0){

		printf("Invalid command\n");

		if(myFlag == TRUE){
			asm_blockCurrentProcess();
			asm_deleteProcess(myPid);
			asm_yield();
		}else{
			unBlockShell();
			asm_deleteProcess(myPid);
			asm_yield();
		}
	}

	/*printf("\nSupported Commands:\n");
	
	for (int i = 1; i < TOTAL_COMMANDS; ++i){
		if(i != HELP){
			printf("-%s\n", cmd[i]);
		}

	}*/

	printHelpList();

	if(myFlag == TRUE){
		asm_blockCurrentProcess();
		asm_deleteProcess(myPid);
		asm_yield();
	}else{
		unBlockShell();
		asm_deleteProcess(myPid);
		asm_yield();
	}
}

void unBlockShell(void){
	atomic();
	asm_unblockProcess(shellPID);
	asm_blockCurrentProcess();
	unAtomic();
}

void executeSetTime(void){
	int myFlag = flag;
	char c1[3];
	char c2[3];
	char c3[3];
	char * args[]={c1, c2, c3};
	int countArgs;

	if(myFlag == TRUE){
		countArgs = splitArgs(shellBuffer+sizeof(char), SETTIME_LENGTH+1, args, 3);
	}else{
		countArgs = splitArgs(shellBuffer, SETTIME_LENGTH+1, args, 3);	
	}

	int myPid = asm_getCurrentPID();
	if (countArgs!=3){
		printf("Invalid command\n");
		if(myFlag == TRUE){
			asm_blockCurrentProcess();
			asm_deleteProcess(myPid);
			asm_yield();
		}else{
			unBlockShell();
			asm_deleteProcess(myPid);
			asm_yield();
		}
	}

	int count1;
	int count2;
	int count3;
	count1 = atoi(args[0]);

	if(count1>23 || count1<0){
		printf("Invalid command\n");
		if(myFlag == TRUE){
			asm_blockCurrentProcess();
			asm_deleteProcess(myPid);
			asm_yield();
		}else{
			unBlockShell();
			asm_deleteProcess(myPid);
			asm_yield();
		}
	}

	count2 = atoi(args[1]);

	if (count2>60 || count2<0){
		printf("Invalid command\n");
		if(myFlag == TRUE){
			asm_blockCurrentProcess();
			asm_deleteProcess(myPid);
			asm_yield();
		}else{
			unBlockShell();
			asm_deleteProcess(myPid);
			asm_yield();
		}
	}

	count3 = atoi(args[2]);

	if (count3>60 || count3<0){
		printf("Invalid command\n");
		if(myFlag == TRUE){
			asm_blockCurrentProcess();
			asm_deleteProcess(myPid);
			asm_yield();
		}else{
			unBlockShell();
			asm_deleteProcess(myPid);
			asm_yield();
		}

	}
	
	count1+=(count1/10)*6;
	count2+=(count2/10)*6;
	count3+=(count3/10)*6;
	
	setTime(HOURS, count1);
	setTime(MINUTES, count2);
	setTime(SECONDS, count3);
	if(myFlag == TRUE){
		asm_blockCurrentProcess();
		asm_deleteProcess(myPid);
		asm_yield();
	}else{
		unBlockShell();
		asm_deleteProcess(myPid);
		asm_yield();
	}
	
}

void executeViewTime(void){
	int myFlag = flag;
	int end = 8;
	if(myFlag == TRUE){
		end++;
	} 
	if(shellBuffer[end]!=0){
		printf("Invalid command\n");
		int myPid = asm_getCurrentPID();
		if(myFlag == TRUE){
			asm_blockCurrentProcess();
			asm_deleteProcess(myPid);
			asm_yield();
		}else{
			unBlockShell();
			asm_deleteProcess(myPid);
			asm_yield();
		}
		return;
	}
    viewTime();
	int myPid = asm_getCurrentPID();
	if(myFlag == TRUE){
		asm_blockCurrentProcess();
		asm_deleteProcess(myPid);
		asm_yield();
	}else{
		unBlockShell();
		asm_deleteProcess(myPid);
		asm_yield();
	}
}

void viewTime(void){
    char hours[3];
    char minutes[3];
    char seconds[3];
    readTime(HOURS, hours);
    readTime(MINUTES, minutes);
    readTime(SECONDS, seconds);
	printf("    %s:%s:%s\n",hours, minutes, seconds);
   
}

void executeSetSSTime(void){
	int myFlag = flag;
	int count;
	if(myFlag == TRUE){
		count = atoi(shellBuffer+sizeof(char)+SETSSTIME_LENGTH+1);
	}else{			
		count = atoi(shellBuffer+SETSSTIME_LENGTH+1);
	}
	if (count<0){
		printf("Invalid command\n");
		int myPid = asm_getCurrentPID();
		if(myFlag == TRUE){
			asm_blockCurrentProcess();
			asm_deleteProcess(myPid);
			asm_yield();
		}else{
			unBlockShell();
			asm_deleteProcess(myPid);
			asm_yield();
		}
	}
	setSSTime(count);
	int myPid = asm_getCurrentPID();
	if(myFlag == TRUE){
		asm_blockCurrentProcess();
		asm_deleteProcess(myPid);
		asm_yield();
	}else{
		unBlockShell();
		asm_deleteProcess(myPid);
		asm_yield();
	}
}

void executeClear(void){
	int myFlag = flag;
	clear();
	int myPid = asm_getCurrentPID();
	if(myFlag == TRUE){
		asm_blockCurrentProcess();
		asm_deleteProcess(myPid);
		asm_yield();
	}else{
		unBlockShell();
		asm_deleteProcess(myPid);
		asm_yield();
	}
}

void executeIpcs(void){
	int myFlag = flag;
	int amountSemid;
	int amountShmid;
	int * ipcssemid = (int *)asm_listSemid(&amountSemid);
	int * ipcsshmid = (int *)asm_listShmid(&amountShmid);
	int myPid = asm_getCurrentPID();

	if( amountSemid == 0 ){
		printf("No ipc structs to show.\n\n");
		if(myFlag == TRUE){
			asm_blockCurrentProcess();
			asm_deleteProcess(myPid);
			asm_yield();
		}else{
			unBlockShell();
			asm_deleteProcess(myPid);
			asm_yield();
		}
	} else {

		printf("Ipcs structs:\n");
		
		for(int j=0; j<amountSemid; j++){
			printf("\nSemaphore \n{\n");
			// printf("      semid : %d\n", ipcs[j].semid);
			// printf("      counter : %d\n", ipcs[j].counter);
			// printf("      blockProcess : %d\n", ipcs[j].blockProcess);
			printf("      semid : %d\n", ipcssemid[j]);
			printf("}\n\n");
		}
	}

	if( amountShmid == 0 ){
		printf("No ipc structs to show.\n\n");
		if(myFlag == TRUE){
			asm_blockCurrentProcess();
			asm_deleteProcess(myPid);
			asm_yield();
		}else{
			unBlockShell();
			asm_deleteProcess(myPid);
			asm_yield();
		}
	} else {
		
		for(int j=0; j<amountShmid; j++){
			printf("\nShared Memory \n{\n");
			// printf("      semid : %d\n", ipcs[j].semid);
			// printf("      counter : %d\n", ipcs[j].counter);
			// printf("      blockProcess : %d\n", ipcs[j].blockProcess);
			printf("      shmid : %d\n", ipcsshmid[j]);
			printf("}\n\n");
		}
	}

	if(myFlag == TRUE){
		asm_blockCurrentProcess();
		asm_deleteProcess(myPid);
		asm_yield();
	}else{
		unBlockShell();
		asm_deleteProcess(myPid);
		asm_yield();
	}
}

void executePs(void){
	int myFlag = flag;
	int psAmount = asm_getProcessesAmount();
	ProcessCharcs * ps = (ProcessCharcs *)asm_listProcesses();
	char * state, * isForeground;

	int sizeName=14, sizeState=9, sizeFg=14;

	printf("---------------------------------------------------------------\n");
	printf("  PID  |     name     |  state  |  foreground  |  memory space\n");
	printf("---------------------------------------------------------------\n");
	
	for(int i=0; i<psAmount ; i++){
		printf("   %d   ", ps[i].pid);
		
		printf(" %s", ps[i].name);
		printSpace( sizeName - (strlen(ps[i].name)) );

		switch(ps[i].state)
		{
			case RUNNING: state = "running"; break;
			case READY: state = "ready"; break;
			case BLOCK: state = "blocked"; break;
		}
		printf("   %s", state);
		printSpace( sizeState - (strlen(state)) );
		
		switch(ps[i].isForeground)
		{
			case TRUE: isForeground = "yes"; break;
			case FALSE: isForeground = "no"; break;
		}
		printf(" %s", isForeground);
		printSpace( sizeFg - strlen(isForeground) );

		printf("%dk\n", ps[i].memorySpace);
	}
	printf("\n");
	int myPid = asm_getCurrentPID();
	if(myFlag == TRUE){
		asm_blockCurrentProcess();
		asm_deleteProcess(myPid);
		asm_yield();
	}else{
		unBlockShell();
		asm_deleteProcess(myPid);
		asm_yield();
	}
}

void printSpace(int amount){
	while(amount>0){
		printf(" ");
		amount--;
	}
}

void executeProdCons(void){
	int myFlag = flag;
	if(myFlag == TRUE){
		repetitions = atoi(shellBuffer+10);
	}else{
		repetitions = atoi(shellBuffer+9);
	}

	if (repetitions < 1){
		printf("Invalid command\n");
		unBlockShell();
		asm_yield();
		return;
	}

	int myPid = asm_getCurrentPID();

	printf("--------------------------------------------------\n");
	printf("        PRODUCER        |        CONSUMER\n");
	printf("--------------------------------------------------\n");

	
	atomic();
	int consumerPID = asm_createProcess((void*)consumer, "consumer", myPid);
	int producerPID = asm_createProcess((void*)producer, "producer", myPid);
	asm_blockCurrentProcess();
	unAtomic();
	asm_yield();

	printf("--------------------------------------------------\n\n");
	
	asm_deleteProcess(consumerPID);
	asm_deleteProcess(producerPID);

	if(myFlag == TRUE){
		asm_blockCurrentProcess();
		asm_deleteProcess(myPid);
		asm_yield();
	}else{
		unBlockShell();
		asm_deleteProcess(myPid);
		asm_yield();
	}
}

void producer(void){
	int idSemProd = asm_semget(KEY_SEM_PROD,0);
	int idSemCons = asm_semget(KEY_SEM_CONS,0);
	int * pointer = asm_shmget(KEY_SHM,INITSPACE);
	int memID = asm_shmat((void*)pointer);
	int i;
	for(i = 1; i<=repetitions; i++){
		sleep(1);
		printf("    Produces: %d", i);
		printf("      ------------> ");

		*pointer = i;
		atomic();
		asm_semUp(idSemCons);
		asm_semDown(idSemProd);
		unAtomic();
		asm_yield();
	}
	int ppid = asm_getppid();
	asm_unblockProcess(ppid);
	asm_yield();
}

void consumer(void){
	int idSemProd = asm_semget(KEY_SEM_PROD,0);
	int idSemCons = asm_semget(KEY_SEM_CONS,0);
	int * pointer = asm_shmget(KEY_SHM,INITSPACE);
	int memID = asm_shmat((void*)pointer);
	int i;
	atomic();
	asm_semDown(idSemCons);
	unAtomic();
	asm_yield();
	for(i = 1; i<=repetitions; i++){
		sleep(1);
		printf("Consumes: %d\n", *pointer);
		atomic();
		asm_semUp(idSemProd);
		asm_semDown(idSemCons);
		unAtomic();
		asm_yield();
	}
	int ppid = asm_getppid();
	asm_unblockProcess(ppid);
	asm_yield();
}


/*
* Receives: 
* 	buf: Buffer from where arguments are read. 
* 	from: Indicates from where to start reading the buffer
* 	args: Destination array, where the arguments are going to be copied.
* Returns: Total arguments read
*/

int splitArgs(char* buf, int from, char** args, int countParam){
	int index = from;
	int currentIndex = 0;
	int argsIndex = 0;
	while(buf[index]!=0){
		if(buf[index]==':'){
			args[argsIndex][currentIndex]=0;
			currentIndex=0;
			argsIndex++;
		}else{
			args[argsIndex][currentIndex]=buf[index];
			currentIndex++;
	

		}
		if(argsIndex>=countParam){
			return -1;
		} 
		index++;
	}
	args[argsIndex][currentIndex]=0;
	argsIndex++;
	if (argsIndex!= countParam){
		return -1;
	}
	return argsIndex;
}

int scanf(){
	asm_setMeOnForeground();
	asm_yield();
	int i;
	if(flag == TRUE){
		flag = FALSE;
	}
	for(i=0; i<SHELL_BUFF_SIZE; i++){
		shellBuffer[i] = 0;
	}
	unsigned char index = 0;
	while((shellBuffer[index % SHELL_BUFF_SIZE] = getChar())!='\n'){
		if(shellBuffer[index % SHELL_BUFF_SIZE] != '\b'){
			index++;
		}
		else{
			if (index>0){
				index--;
			}
		}			
	}
	asm_resetBuffer();
	asm_freeForeground();
	shellBuffer[index % SHELL_BUFF_SIZE]=0;
	printChar('\n');
	return index;
}
