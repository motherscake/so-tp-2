GLOBAL read
GLOBAL write
GLOBAL readTime
GLOBAL setTime
GLOBAL setSSTime
GLOBAL clear
GLOBAL asm_alloc
GLOBAL asm_free
GLOBAL asm_createProcess
GLOBAL asm_deleteProcess
GLOBAL asm_listProcesses
GLOBAL asm_blockProcess
GLOBAL asm_unblockProcess
GLOBAL asm_blockCurrentProcess
GLOBAL asm_deleteCurrentProcess
GLOBAL asm_getProcessesAmount
GLOBAL atomic
GLOBAL unAtomic
GLOBAL asm_yield
GLOBAL asm_getCurrentPID
GLOBAL asm_setMeOnForeground
GLOBAL asm_freeForeground
GLOBAL asm_shmget
GLOBAL asm_shmat
GLOBAL asm_shmdt
GLOBAL asm_semget
GLOBAL asm_semctl
GLOBAL asm_notShow
GLOBAL asm_resetBuffer
GLOBAL asm_semDown
GLOBAL asm_semUp
GLOBAL asm_listSemid
GLOBAL asm_listShmid
GLOBAL asm_getIpcsAmount
GLOBAL asm_getppid

atomic:
	cli
	ret

unAtomic:
	sti
	ret

asm_alloc:
	push rbp
	mov rbp, rsp
	mov rcx, 7
	int 0x80
	mov rsp, rbp
	pop rbp
	ret


asm_free:
	push rbp
	mov rbp, rsp
	mov rcx, 8
	int 0x80
	mov rsp, rbp
	pop rbp
	ret

setSSTime:
	push rbp
	mov rbp, rsp
	mov rcx, 5
	int 0x80
	mov rsp, rbp
	pop rbp
	ret

clear:
	push rbp
	mov rbp, rsp
	mov rcx, 6
	int 0x80
	mov rsp, rbp
	pop rbp
	ret

read:
	push rbp
	mov rbp, rsp
	mov rcx, 1

	int 0x80

	mov rsp, rbp
	pop rbp
	ret

write:
	push rbp
	mov rbp, rsp
	mov rcx, 2
	int 0x80
	mov rsp, rbp
	pop rbp
	ret

readTime:
	push rbp
	mov rbp, rsp
	mov rdx, 2
	mov rcx, 3
	int 0x80
	mov rsp, rbp
	pop rbp
	ret	


;setTime:
;rdi: timeRegister
;rsi: time
;rdx: function Code
;rcx: unknown

setTime:
	push rbp
	mov rbp, rsp

	mov rdx, 3
	mov rcx, 4
	int 0x80
	
	mov rsp, rbp
	pop rbp
	ret

asm_createProcess:
	push rbp
	mov rbp, rsp
	mov rcx, 9
	int 0x80
	mov rsp, rbp
	pop rbp
	ret

asm_deleteProcess:
	push rbp
	mov rbp, rsp
	mov rcx, 10
	int 0x80
	mov rsp, rbp
	pop rbp
	ret


asm_deleteCurrentProcess:
	push rbp
	mov rbp, rsp
	mov rcx, 14
	int 0x80
	mov rsp, rbp
	pop rbp
	ret


asm_blockCurrentProcess:
	push rbp
	mov rbp, rsp
	mov rcx, 15
	int 0x80
	mov rsp, rbp
	pop rbp
	ret


asm_listProcesses:
	push rbp
	mov rbp, rsp
	mov rcx, 11
	int 0x80
	mov rsp, rbp
	pop rbp
	ret


asm_getProcessesAmount:
	push rbp
	mov rbp, rsp
	mov rcx, 20
	int 0x80
	mov rsp, rbp
	pop rbp
	ret

asm_blockProcess:
	push rbp
	mov rbp, rsp
	mov rcx, 12
	int 0x80
	mov rsp, rbp
	pop rbp
	ret

asm_unblockProcess:
	push rbp
	mov rbp, rsp
	mov rcx, 13
	int 0x80
	mov rsp, rbp
	pop rbp
	ret

asm_yield:
	push rbp
	mov rbp, rsp
	mov rcx, 16
	int 0x80
	mov rsp, rbp
	pop rbp
	ret

asm_getCurrentPID:
	push rbp
	mov rbp, rsp
	mov rcx, 17
	int 0x80
	mov rsp, rbp
	pop rbp
	ret

asm_setMeOnForeground:
	push rbp
	mov rbp, rsp
	mov rcx, 18
	int 0x80
	mov rsp, rbp
	pop rbp
	ret

asm_freeForeground:
	push rbp
	mov rbp, rsp
	mov rcx, 19
	int 0x80
	mov rsp, rbp
	pop rbp
	ret

asm_shmget:
	push rbp
	mov rbp, rsp
	mov rcx, 21
	int 0x80
	mov rsp, rbp
	pop rbp
	ret

asm_shmat:
	push rbp
	mov rbp, rsp
	mov rcx, 22
	int 0x80
	mov rsp, rbp
	pop rbp
	ret

asm_shmdt:
	push rbp
	mov rbp, rsp
	mov rcx, 23
	int 0x80
	mov rsp, rbp
	pop rbp
	ret

asm_semget:
	push rbp
	mov rbp, rsp
	mov rcx, 24
	int 0x80
	mov rsp, rbp
	pop rbp
	ret

asm_semctl:
	push rbp
	mov rbp, rsp
	mov rcx, 25
	int 0x80
	mov rsp, rbp
	pop rbp
	ret

asm_notShow:
	push rbp
	mov rbp, rsp
	mov rcx, 26
	int 0x80
	mov rsp, rbp
	pop rbp
	ret


asm_resetBuffer:
	push rbp
	mov rbp, rsp
	mov rcx, 27
	int 0x80
	mov rsp, rbp
	pop rbp
	ret

asm_semDown:
	push rbp
	mov rbp, rsp
	mov rcx, 28
	int 0x80
	mov rsp, rbp
	pop rbp
	ret

asm_semUp:
	push rbp
	mov rbp, rsp
	mov rcx, 29
	int 0x80
	mov rsp, rbp
	pop rbp
	ret


asm_listSemid:
	push rbp
	mov rbp, rsp
	mov rcx, 30
	int 0x80
	mov rsp, rbp
	pop rbp
	ret

asm_listShmid:
	push rbp
	mov rbp, rsp
	mov rcx, 33
	int 0x80
	mov rsp, rbp
	pop rbp
	ret

asm_getIpcsAmount:
	push rbp
	mov rbp, rsp
	mov rcx, 31
	int 0x80
	mov rsp, rbp
	pop rbp
	ret


asm_getppid
	push rbp
	mov rbp, rsp
	mov rcx, 32
	int 0x80
	mov rsp, rbp
	pop rbp
	ret
