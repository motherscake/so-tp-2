#ifndef VIDEO_H_
#define VIDEO_H_

#include <stdint.h>

#include "types.h"

#define DIR_VIDEO 0xB8000

#define TAB_SPACES 7

#define STDOUT 0

int sysWrite(int fd,const char* buffer, int lenght);

void kPrintChar(char character, char attr);

void kPrintNull(char * str);

void kPrintColorString(const char * string, char attr);

void kPrint(const char* string, int length);

void kNewline();

void kPrintDec(uint64_t value);

void kPrintHex(uint64_t value);

void kPrintBin(uint64_t value);

void kPrintBase(uint64_t value, uint32_t base);

void clearScreen();

void scroll();

void applyScreenSaver();

void resetScreen(char* b, uint8_t* dir);

uint8_t * backupScreen(char* b);

/////////TODELETE//////////////

void printAux(void);

//////////////////////////////

#endif