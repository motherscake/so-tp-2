
#ifndef RTC_
#define RTC_

/*
 * Reads the time: HOURS, MINUTES, SECONDS
*/
char sysRTCRead(char timeRegister);


/*
 * Sets the time: HOURS, MINUTES, SECONDS
*/
void sysRTCSet(char timeRegister, char value);

#endif /* RTC_ */