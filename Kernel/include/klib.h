
#ifndef KLIB_H_
#define KLIB_H_
#include <stdint.h>
#define TRUE 1
#define FALSE 0

void * memset(void * destination, int32_t character, uint64_t length);

void * memcpy(void * destination, const void * source, uint64_t length);

void itoa(int num, char arr[]);

int atoi(char * arr);

/*
 * Reads a value in BCD format and returns it's string representation in the given array.		
*/
void readBCD(char value, char * retArr);

#endif