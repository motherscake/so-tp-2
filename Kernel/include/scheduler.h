#ifndef SCHEDULER_H
#define SCHEDULER_H


#include "processSlot.h"
#include "allocator.h"

void * switchUserToKernel(void * esp);
void * switchKernelToUser(void);
void * getCurrentEntryPoint(void);
// private:
// 	Log & log;

//Scheduler(Log & log);
void schedule(void);
void addProcess(Process * process);
void removeProcess(Process * process);
Process * getProcessByPID(int pid);
ProcessCharcs * listProcesses(void);
int getProcessesAmount(void);

void initScheduler(void);
Process * getForegroundProcess(void);
void setMeForegroundProcess(void);
// void blockMe();
void setForegroundProcess(Process * process);
void blockCurrentProcess(void);
void pushQueue(void);
void popQueue(void);
void freeForeground(void);
int getForegroundProcessPID(void);
int getCurrentPid();
uint64_t * getCurrentNextAlloc();
int getCurrentppid(void);
void incMemorySpace(uint64_t kb);
void decMemorySpace(uint64_t kb);


#endif