#ifndef PROCESS_H
#define PROCESS_H

typedef enum {RUNNING, READY, BLOCK} state;

#include "allocator.h"
#include "types.h"
#include <stdint.h>


typedef struct {
	//Registers restore context
	uint64_t gs;
	uint64_t fs;
	uint64_t r15;
	uint64_t r14;
	uint64_t r13;
	uint64_t r12;
	uint64_t r11;
	uint64_t r10;
	uint64_t r9;
	uint64_t r8;
	uint64_t rsi;
	uint64_t rdi;
	uint64_t rbp;
	uint64_t rdx;
	uint64_t rcx;
	uint64_t rbx;
	uint64_t rax;
	//iretq hook
	uint64_t rip;
	uint64_t cs;
	uint64_t eflags;
	uint64_t rsp;
	uint64_t ss;
	uint64_t base;
} StackFrame;

typedef struct 
{
	int pid;
	int ppid;
	state state;
	uint64_t * nextAlloc; //Points to the next Heap page to be allocated
	uint64_t * userStack;
	uint64_t * kernelStack;
	char * name;
	int memorySpace;

	void * entryPoint;
	uint64_t * userStackPage;
	uint64_t * kernelStackPage;

	/* ADDED FOR PAGING */
	//void * heap[HEAP_MAX] = {0}; //Ask for the current process and add the page to heap
	//May be useful for future implementations
	//void * currentHeapPage;
} Process;


typedef struct
{
	int pid;
	state state;
	char * name;
	int memorySpace;
	int isForeground;

} ProcessCharcs;

Process * newProcess(void * entryPoint, char * name, int ppid);

void deleteProcess(Process * process);
static void * toStackAddress(void * page);
StackFrame * fillStackFrame(void * entryPoint, Process * process);


#endif