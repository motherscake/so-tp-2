#ifndef BUDDY_MEMORY_ALLOCATION_H
#define BUDDY_MEMORY_ALLOCATION_H

#include "types.h"
#include "klib.h"
#include <stdint.h>
#define TRUE 1
#define FALSE 0
#define PAGE_SIZE 0x1000

/* ADDED FOR PAGING */ //Changed from 0x600000
#define BASE_ADDR 0x800000

//#define assert(expression) if (!expression) return FALSE;


//struct buddy * buddy_new(int level);
//void buddy_delete(struct buddy *);
void * buddy_alloc(int size);
int buddy_free(void * pointer);
void initializeAllocator(void);
int sysFree(void * pointer);
void * sysAlloc(int size);



#endif