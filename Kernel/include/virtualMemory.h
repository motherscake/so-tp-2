#ifndef VIRTUAL_MEMORY_H_
#define VIRTUAL_MEMORY_H_
/* ADDED FOR PAGING */

#include <types.h>
#define GIGABYTE ((uint64_t)0x40000000)

extern void setUpPaging(void);
extern void initializePaging();
uint64_t * getPhysicalPage(uint64_t * virtualPage);
uint64_t * getVirtualPage(uint64_t size);
void handlePageFault(uint64_t error, uint64_t page);
int freeVirtualPage(uint64_t * virtualPage);


static const uint64_t PageSize = 0x1000;

/* Cada tabla es de 4K, conteniendo entradas de 64bits(8 bytes) -> Tiene 512 entradas*/
static uint64_t * PML4 = (uint64_t *) 0x700000;
static uint64_t * PDPT = (uint64_t *) 0x701000; //La base es 0x701000, pero en el puntero de PML4, se le pasa con un 7 al final.
//static uint64_t * PDT = (uint64_t *) 0x702000; //TEST
//static uint64_t * PT = (uint64_t *) 0x703000;  //TEST

#endif /* VIRTUAL_MEMORY_H_ */