
#ifndef HANDLERS_H_
#define HANDLERS_H_

#include "types.h"
#include "RTC.h"
#include "video.h"
#include "klib.h"
#include "allocator.h"
#include "sharedMemory.h"
#include "scheduler.h"
#include "process.h"
#include "interrupts.h"

#define HOURS 0x04
#define MINUTES 0x02
#define SECONDS 0x00

#define SYS_READ 1
#define SYS_WRITE 2
#define SYS_RTC_READ 3
#define SYS_RTC_SET 4
#define SYS_SSTIME 5
#define SYS_CLEAR 6
#define SYS_ALLOC 7
#define SYS_FREE 8
#define SYS_CREAT_PROC 9
#define SYS_DELETE_PROC 10
#define SYS_LIST_PROC 11
#define SYS_BLOCK_PROC 12
#define SYS_UNBLOCK_PROC 13
#define SYS_DELETE_CURR_PROC 14
#define SYS_BLOCK_CURR_PROC 15
#define SYS_YIELD 16
#define SYS_GET_CURRENT_PID 17
#define SYS_SET_FOREGROUND 18
#define SYS_FREE_FOREGROUND 19
#define SYS_AMOUNT_PROC 20
#define SYS_SHMGET 21
#define SYS_SHMAT 22
#define SYS_SHMDT 23
#define SYS_SEMGET 24
#define SYS_SEMCTL 25
#define SYS_NOT_SHOW 26
#define SYS_RESET_BUFFER 27
#define SYS_SEM_DOWN 28
#define SYS_SEM_UP 29
#define SYS_LIST_SEM_ID 30
#define SYS_AMOUNT_IPCS 31
#define SYS_GET_PPID 32
#define SYS_LIST_SHM_ID 33

int sysCallHandler(uint64_t arg1, uint64_t arg2, uint64_t arg3, uint64_t arg4);


/*
 * Function Code 2: Reading RTC.
 *		timeRegister: Register to be read (HOURS, MINUTES, SECONDS).
 *		arg2: Buffer for returning the value read.
 * Function Code 3: Setting RTC.
 *		timeRegister: Register to be set (HOURS, MINUTES, SECONDS).
 *		arg2: time value to set.
*/
//TODO: DELETE: void RTCHandler(int timeRegister, int arg2, int functionCode);

#endif /* HANDLERS_H_ */
