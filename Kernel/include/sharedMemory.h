#ifndef SHAREDMEMORY_H_
#define SHAREDMEMORY_H_

#include <types.h>
#include <processSlot.h>
#include <scheduler.h>
#define AMOUNT 100
#define ERRORCODE -1

typedef struct {
    
   int pid;
   void * next;

} SemNode;

typedef struct {

    int counter;
    int blockProcess;
    SemNode * first;
    SemNode * last;

} Semaphore;

typedef struct {
	
    int semid;
	int counter;
    int blockProcess;
	
} SemaphCharcs;



int shmget(int key, int memory);
int shmat(void * pointer);
void shmdt(int idmem);
int semget(int key, int initState);
void down(int semid);
void pushQueueSem(Semaphore * sem);
void up (int semid);
void popQueueSem(Semaphore * sem);
void semctl(int semid);
void SemFree(Semaphore * sem);
int * listsemid(int * amount);
int * listshmid(int * amount);

#endif /* SHAREDMEMORY_H_ */