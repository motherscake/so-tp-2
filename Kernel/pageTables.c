/* ADDED FOR PAGING */
#include <virtualMemory.h>
#include <allocator.h>
#include <video.h>
#include <klib.h>
#include <scheduler.h>


#define PDPT_NOT_PRESENT 0
#define PDT_NOT_PRESENT 1
#define PT_NOT_PRESENT 2
#define PAGE_NOT_PRESENT 3


void initializePaging(){

	memset(PML4, 0,  PageSize * 6);
	
	PML4[0] = (uint64_t)PDPT | 7; //Map 512GB
	
	PDPT[0] = 0x0 | 0x87; //Identity Map first GB for kernel and user code&data

	setUpPaging();
	
}

void handlePageFault(uint64_t errorCode, uint64_t page){

	uint64_t physicalPage;
	
	int pid = getCurrentPid();

	uint64_t PML4Index = (uint64_t)page / (0x8000000000);
	uint64_t PDPTIndex = (uint64_t)page % (0x8000000000) / (0x40000000);
	uint64_t PDTIndex = (uint64_t)page % (0x40000000) / (0x200000);
	uint64_t PTIndex = (uint64_t)page % 0x200000 / 0x1000;

	uint64_t * PDT = (uint64_t *)(PDPT[PDPTIndex] & 0xFFFFFFFFFFFFF000); //La PDPT esta fija en el 0x702000
	uint64_t * PT;
	uint64_t * auxPage;
	
	if(  page > ( GIGABYTE * (pid + 1) ) || page < ( GIGABYTE * (pid + 1) ) - 0x800000 ){
		kPrint("PAGE FAULT EXCEPTION", 20);
		while(1);
	}

 	physicalPage = getPhysicalPage(page);
 	
 	switch(physicalPage){

 		case PDPT_NOT_PRESENT:

 		case PDT_NOT_PRESENT:

 			kPrint("PAGE FAULT EXCEPTION", 20);
 			while(1);
 			
 			break;

 		case PT_NOT_PRESENT:

 			//kPrint("In\n", 3);

 			PT = (uint64_t *) sysAlloc(PAGE_SIZE);
 			memset(PT, 0, PageSize);
			
			auxPage = (uint64_t *) sysAlloc(PAGE_SIZE);
			memset(auxPage, 0, PageSize);

			PDPT[PDPTIndex] = (uint64_t)PDT | 7;
			PDT[PDTIndex] = (uint64_t)PT | 7; 
			PT[PTIndex] = (uint64_t)auxPage | 7;


 			break;

 		case PAGE_NOT_PRESENT:

			auxPage = (uint64_t *) sysAlloc(PAGE_SIZE);
			memset(auxPage, 0, PageSize);

			PT = (uint64_t *)(PDT[PDTIndex] & 0xFFFFFFFFFFFFF000);
			PT[PTIndex] = (uint64_t)auxPage | 7; 

 			break;
 		default:
 			kPrint("PAGE FAULT EXCEPTION", 20);
 			while(1);
 			break;						

 	}

 	setUpPaging();

}



uint64_t * getPhysicalPage(uint64_t * virtualPage){

	uint64_t PML4Index, PDPTIndex, PDTIndex, PTIndex;

	uint64_t * PDPTAddr, * PDTAddr, * PTAddr, * pageAddr;

	PML4Index = (uint64_t)virtualPage / (0x8000000000);
	PDPTIndex = (uint64_t)virtualPage % (0x8000000000) / (0x40000000);
	PDTIndex = (uint64_t)virtualPage % (0x40000000) / (0x200000);
	PTIndex = (uint64_t)virtualPage % 0x200000 / 0x1000;

	if(PML4[PML4Index] % 2 == 0){
		return PDPT_NOT_PRESENT; //PDPT not present
	}

	PDPTAddr = (uint64_t *)(PML4[PML4Index] & 0xFFFFFFFFFFFFF000);

	if(PDPTAddr[PDPTIndex] % 2 == 0){
		return PDT_NOT_PRESENT; //PDT not present
	}

	PDTAddr = (uint64_t *)(PDPTAddr[PDPTIndex] & 0xFFFFFFFFFFFFF000);

	if(PDTAddr[PDTIndex] % 2 == 0){
		return PT_NOT_PRESENT; //PT not present
	}

	PTAddr = (uint64_t *)(PDTAddr[PDTIndex] & 0xFFFFFFFFFFFFF000);

	if(PTAddr[PTIndex] % 2 == 0){
		return PAGE_NOT_PRESENT; //Page not present
	}

	pageAddr = (uint64_t *)(PTAddr[PTIndex] & 0xFFFFFFFFFFFFF000);
	return pageAddr;
}

uint64_t * getVirtualPage(uint64_t size){

	int pid = getCurrentPid();
	uint64_t * page = getCurrentNextAlloc();
	
	uint64_t * PT, auxPage;
	uint64_t * PDT = (uint64_t *)(PDPT[pid] & 0xFFFFFFFFFFFFF000);
	
	uint64_t PDTIndex = (uint64_t)page % (0x40000000) / (0x200000);
	uint64_t PTIndex = (uint64_t)page % 0x200000 / 0x1000;

	if(!(PDT[PDTIndex]%2)){ //Si no esta creada la PT, la creo

		PT = sysAlloc(PageSize);
		memset(PT,0,0x1000); 
		PDT[PDTIndex] = (uint64_t)PT | 7;

	}else{
		PT = PDT[PDTIndex] & 0xFFFFFFFFFFFFF000;
	}
	PT[PTIndex] = (uint64_t)sysAlloc(PageSize) | 7;  //Siempre creo la pagina

	setUpPaging();

	incMemorySpace(4);

	return page;
}


int freeVirtualPage(uint64_t * virtualPage){

	uint64_t * physicalPage = getPhysicalPage(virtualPage);

	if(physicalPage <= 3){ //If error occurrs return -1
		return -1;
	}
	decMemorySpace(4);
	return sysFree(physicalPage);
}