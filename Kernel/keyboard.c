#include <video.h>
#include <keyboard.h>
#include <scheduler.h>
#include <interrupts.h>

static int SSTime = 30 * 10000;
static int shift = 0;
static int ctrl = 0;
static char buf[BUFF_SIZE]={0};
static int read =0;
static int write =0;


/* Variables used for the Screen Saver*/
static long long amount = 0;
static int backupCheck = 0;
static uint8_t * oldCurrent;
static char backup[SCREEN_SIZE]={0};


static int show = TRUE;

char * keyboard(unsigned char code){

    if (amount>SSTime){
        resetScreen(backup, oldCurrent);
        backupCheck = 0;
        amount = 0;
        return (char*)NOT_PRINTABLE;
    }
    amount = 0;

	switch(code){
		case SHIFTL_MAKE:
			shift = 1;
			break;
		case SHIFTR_MAKE:
			shift = 1;
			break;
		case CTRL_MAKE:
			ctrl = 1;
			break;
		case SHIFTL_BREAK:
			shift = 0;
			break;
		case SHIFTR_BREAK:
			shift = 0;
			break;
		case CTRL_BREAK:
			ctrl = 0;
			break;
		default:
			if( (code < 0x80) && (ctrl == 0) && 
                scanCodes[code][shift] != NOT_PRINTABLE){ //If its a makecode
                
                    buf[write % BUFF_SIZE] = scanCodes[code][shift];
                    write++;

                    if(scanCodes[code][shift]=='\n'){

                        Process * process = getForegroundProcess();
                        process->state = READY;
                        show = TRUE;
                        
                    }
                    if(show == TRUE){
                        kPrint(scanCodes[code]+shift,1);
                    }
                    return &(scanCodes[code][shift]);    
                }
			
            break;

		}     	
	return (char*)NOT_PRINTABLE;
}


 /*Copy from keyboard buffer to the given buffer*/
int sysRead(int fd, char* buffer, int lenght){
    int i = 0;
    if( fd == STDIN){

        if(lenght == 0){
            while(buf[read % BUFF_SIZE] != '\n'){
                while(read%BUFF_SIZE==write%BUFF_SIZE);
                buffer[i++] = buf[read % BUFF_SIZE];
                read++;
            }
        }else{

            for(i = 0; i<lenght; i++){
                while(read%BUFF_SIZE==write%BUFF_SIZE);
                buffer[i] = buf[read % BUFF_SIZE];
                read++;
            } 
        }
    
        return i;
    }
    return 0;
}

void screenSaver(){
    if ((amount>SSTime) && (backupCheck==0)){
        oldCurrent = backupScreen(backup);
        clearScreen();
        applyScreenSaver();
        backupCheck =1;
    }else if (backupCheck!=1){
        amount+=55;
    }

}

void kernelSetSSTime(int times){

    if(times == 0){

        SSTime = 60000;

    }
    else{

        SSTime = times * 60000; //Change time to milliseconds
    }   
} 

void notShow(void){
    show = FALSE;
}

void resetBuffer(void){
    read = write;
}