//sharedMemory.c

#include <sharedMemory.h>
#include <scheduler.h>

static int shmAmount = 0;
static int shmId [AMOUNT] = {0};
static void * shmget_Arr [AMOUNT];
static int shmkey [AMOUNT] = {0};
static int idShm = 1;

static int semAmount = 0;
static int semId [AMOUNT] = {0};
static Semaphore * semget_Arr [AMOUNT];
static int semkey[AMOUNT] = {0};
static int idSem = 1;


int shmget(int key, int memory){
    if(memory < 0){
        return ERRORCODE;
    }
    if(AMOUNT == shmAmount){
        return ERRORCODE;
    }
    int empty = -1;
    int i;
    for(i = 0 ; i < AMOUNT ; i++){
        if(shmkey[i] == 0 && empty<0 ){
            empty = i;
        }
        if(shmkey[i] == key){
            return shmget_Arr[i];
        }
    }
    void * pointer = sysAlloc(memory);
    shmkey[empty] = key;
    shmget_Arr[empty] = pointer;
    shmAmount++;
    return pointer;
}

int shmat(void * pointer){
    int i;
    for(i = 0 ; i < AMOUNT ; i++){
        if(shmget_Arr[i] == pointer){
            if(shmId[i] == 0){
                shmId[i] = idShm;
                idShm++;
                return idShm-1;
            }else{
                return shmId[i];
            }
        }
    }
    return ERRORCODE;
}

void shmdt(int idMem){
    int i;
    for (i = 0; i < AMOUNT; i++){
        if(shmId[i] == idMem){
            sysFree(shmget_Arr[i]);
            shmkey[i] = 0;
            shmId[i] = 0;
            shmAmount--;
        }
    }
    
}

int semget(int key, int initState){

    if(initState <0){
        return ERRORCODE;
    }
    if(AMOUNT == semAmount){
        return ERRORCODE;
    }
    int empty = -1;
    int i;
    for(i = 0 ; i < AMOUNT ; i++){
        if(semkey[i] == 0  && empty<0 ){
            empty = i;
        }
        if(semkey[i] == key){
            return semId[i];
        }
    }
    Semaphore * sem = sysAlloc(sizeof(Semaphore));
    sem->counter = initState;
    sem->blockProcess = 0;
    sem->first = NULL;
    sem->last = NULL;
    semkey[empty] = key;
    semget_Arr[empty] = sem;
    semId[empty] = idSem;
    idSem++;
    semAmount++;
    return idSem-1;
}

void down(int semid){
    int i;
    Semaphore * current;
    for (i = 0; i < AMOUNT; i++){
        if(semId[i] == semid){
            current = semget_Arr[i];
            (current->counter)--;
            if (current->counter < 0){
                pushQueueSem(current);
            }else{
                return;
            }
        }
    }
}

void pushQueueSem(Semaphore * sem){
    blockCurrentProcess();
    int pid = getCurrentPid();
    SemNode * node = sysAlloc(sizeof(SemNode));
    node->pid = pid;
    node->next = NULL;
    if(sem->blockProcess == 0){
        sem->first = node;
        sem->last = node;
    }else{
        sem->last->next = node;
        sem->last = node;
    }
    sem->blockProcess++;
}

void up(int semid){
    int i;
    Semaphore * current;
    for (i = 0; i < AMOUNT; i++){
        if(semId[i] == semid ){
            current = semget_Arr[i];
            (current->counter)++;
            if (current->counter <= 0){
                popQueueSem(current);
            }else{
                return;
            }
        }
    }
}

void popQueueSem(Semaphore * sem){
    int pid;
    pid = sem->first->pid;
    if(sem->blockProcess == 1){
        sysFree(sem->first);
        sem->first = NULL;
        sem->last = NULL;
    }else{ 
        sem->first = (SemNode *) sem->first->next;
        sysFree(sem->first);
    }
    sem->blockProcess--;
    unblockProcess(pid);
}

void semctl(int semid){
    int i;
    for (i = 0; i < AMOUNT; i++){
        if(semId[i] == semid )
            SemFree(semget_Arr[i]);
            sysFree(semget_Arr[i]);
            shmkey[i] = 0;
            shmId[i] = 0;
            semAmount--;
    }
}

void SemFree(Semaphore * sem){
    int i;
    SemNode * aux;
    SemNode * current = sem->first;
    for (i = 0; i < sem->blockProcess; i++){
        unblockProcess(current->pid);
        aux = (SemNode *)current->next;
        sysFree(current);
        current = aux;
    }

}

int * listsemid(int * amount){
    *amount = semAmount;
    int * ans = sysAlloc(semAmount*sizeof(int));

    int index = 0;
    for(int i = 0 ; i < AMOUNT ; i++){
        if( !(semkey[i] == 0) ){
            ans[index] = semId[i];
            index++;
        }
    }
    return ans;
}

int * listshmid(int * amount){
    *amount = shmAmount;
    int * ans = sysAlloc(shmAmount*sizeof(int));

    int index = 0;
    for(int i = 0 ; i < AMOUNT ; i++){
        if( !(shmkey[i] == 0) ){
            ans[index] = shmId[i];
            index++;
        }
    }
    return ans;
}