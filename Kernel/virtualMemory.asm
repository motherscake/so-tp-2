;/* ADDED FOR PAGING */
GLOBAL setUpPaging

%macro pushaq 0
    push rax      ;save current rax
    push rbx      ;save current rbx
    push rcx      ;save current rcx
    push rdx      ;save current rdx
    push rbp      ;save current rbp
    push rdi       ;save current rdi
    push rsi       ;save current rsi
    push r8        ;save current r8
    push r9        ;save current r9
    push r10      ;save current r10
    push r11      ;save current r11
    push r12      ;save current r12
    push r13      ;save current r13
    push r14      ;save current r14
    push r15      ;save current r15
    push fs
    push gs
%endmacro

%macro popaq 0
    pop gs
    pop fs
    pop r15
    pop r14
    pop r13
    pop r12
    pop r11
    pop r10
    pop r9
    pop r8
    pop rsi
    pop rdi
    pop rbp
    pop rdx
    pop rcx
    pop rbx
    pop rax

%endmacro

;TODO: Send pml4 adress in eax

setUpPaging:

	push rbp
    mov rbp, rsp
    pushaq
    
    mov rax, cr3
    and rax, 0xFFF0000000000FFF
	or rax, 0x0000000000700000 ;Point to the 7th MB
	
    ;mov rax, 4

    mov cr3, rax
    

    popaq
    mov rsp, rbp
    pop rbp

    ret