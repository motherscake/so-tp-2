//sheduller.c

#include <scheduler.h>
#include <interrupts.h>
#include <virtualMemory.h>

struct Scheduler {
	ProcessSlot * current;
};

struct QueueBlockForeground {
	ProcessSlot * first;
	ProcessSlot * last;
};

static struct Scheduler scheduler;

static struct QueueBlockForeground blockFQueue;

static int queueSize = 0;

static int processesAmount = 0;

static Process * foreground = NULL;

static int foregroundPID = 0;

void initScheduler(void){

	scheduler.current = NULL;
	blockFQueue.first = NULL;
	blockFQueue.last = NULL;

}


void pushQueue(void){
	ProcessSlot * newProcess = sysAlloc(sizeof(ProcessSlot));
	newProcess->process = scheduler.current->process;
	if (blockFQueue.first == NULL) {
		blockFQueue.first = newProcess;
		blockFQueue.last = newProcess;
	} else {
		//ProcessSlot * next = (ProcessSlot *)scheduler.current->next;
		blockFQueue.last->next = (void *)newProcess;

	}
	queueSize++;
}

void popQueue(void){
	ProcessSlot * current = blockFQueue.first;
	if(queueSize>1){
		blockFQueue.first = blockFQueue.first->next;
	}else{
		blockFQueue.first = NULL;
	}
	foreground = current->process;
	foregroundPID = current->process->pid;
	queueSize--;
	sysFree(current);
}

void freeForeground(void){

		foreground = NULL;
	
}

void * switchUserToKernel(void * esp) {

	Process * process = scheduler.current->process;

	//Backup current process esp in his userStack variable.
	process->userStack = esp;

	return process->kernelStack;

}

void * switchKernelToUser(void) {
	schedule();
	return scheduler.current->process->userStack;
}

void * getCurrentPoint(void) {
	return scheduler.current->process->entryPoint;
}


void schedule(void) {
	int amount = 0;

	if(scheduler.current == (ProcessSlot *)scheduler.current->next){
		return;
	}

	scheduler.current = (ProcessSlot *)scheduler.current->next;
	while(scheduler.current->process->state == BLOCK && amount<processesAmount){
		amount++;
		scheduler.current = (ProcessSlot *)scheduler.current->next;
	}
}

void addProcess(Process * process) {
	ProcessSlot * newProcess = sysAlloc(sizeof(ProcessSlot));
	newProcess->process = process;

	if (scheduler.current == NULL) {
		scheduler.current = newProcess;
		scheduler.current->next = scheduler.current;
	} else {
		ProcessSlot * next = (ProcessSlot *)scheduler.current->next;

		scheduler.current->next = (void *)newProcess;
		newProcess->next = (void *)next;

	}
	processesAmount++;
}

void removeProcess(Process * process) {
	ProcessSlot * prevSlot = scheduler.current;
	ProcessSlot * slotToRemove = (ProcessSlot *)scheduler.current->next;

	if (scheduler.current == NULL) {
		return;
	} else if (prevSlot == slotToRemove && process == scheduler.current->process) {
		deleteProcess(scheduler.current->process);
		sysFree(scheduler.current);
		scheduler.current = NULL;
		processesAmount --;
		return;
	}

	while(slotToRemove->process != process){
		prevSlot = slotToRemove;
		slotToRemove = (ProcessSlot *)slotToRemove->next;
	}
	
	processesAmount--;

	prevSlot->next = slotToRemove->next;

	deleteProcess(slotToRemove->process);
	sysFree(slotToRemove);
}


void removeCurrentProcess(void){
	Process * process = scheduler.current->process;
	scheduler.current = (ProcessSlot *)scheduler.current->next;
	removeProcess(process);
}


Process * getProcessByPID(int pid){
	
	if(pid == scheduler.current->process->pid){
		return scheduler.current->process;
	}
	ProcessSlot * current2 = (ProcessSlot *)scheduler.current->next;
	while(TRUE){
		if(current2 == scheduler.current){
			return NULL;
		}
		if(current2->process->pid == pid){
			return current2->process;
		}
		current2 = (ProcessSlot *)current2->next;	
	}
}

ProcessCharcs * listProcesses(void){
	ProcessCharcs * ans = sysAlloc(processesAmount*(sizeof(ProcessCharcs)+1));
	ProcessSlot * current2 = scheduler.current;

	for (int i = 0; i < processesAmount; i++)
	{
		ans[i].pid = current2->process->pid;
		ans[i].state = current2->process->state;
		ans[i].name = current2->process->name;
		ans[i].memorySpace = current2->process->memorySpace;
		ans[i].isForeground = (current2->process->pid==foregroundPID) ? TRUE : FALSE;
		current2 = (ProcessSlot *)current2->next;
	}
	return ans;
}

int getProcessesAmount(void){
	return processesAmount;
}

void blockProcess(int pid){
	Process * process = getProcessByPID(pid);
	process->state = BLOCK;
}

void unblockProcess(int pid){
	Process * process = getProcessByPID(pid);
	process->state = READY;

}

Process * getForegroundProcess(void){
	return foreground;
}

int getForegroundProcessPID(void){
	return foregroundPID;
}

void setMeForegroundProcess(void){
	while(foreground != NULL){
		_yield();
	}
	scheduler.current->process->state = BLOCK;
	foreground = scheduler.current->process;
	foregroundPID = scheduler.current->process->pid;
}

// void blockMe(){
// 	scheduler.current->process->state = BLOCK;
// 	_yield();
// }

void setForegroundProcess(Process * process){
	foreground = process;
}

void blockCurrentProcess(void){
	scheduler.current->process->state = BLOCK;
}

int getCurrentPid(void){
	return scheduler.current->process->pid;
}

uint64_t * getCurrentNextAlloc(){
	
	uint64_t * page = scheduler.current->process->nextAlloc;
	scheduler.current->process->nextAlloc += PageSize/8;
	return page;
}

int getCurrentppid(void){
	return scheduler.current->process->ppid;
}

void incMemorySpace(uint64_t kb){
	scheduler.current->process->memorySpace += kb;
}

void decMemorySpace(uint64_t kb){
	scheduler.current->process->memorySpace -= kb;
	
	if(scheduler.current->process->memorySpace < 0){
		scheduler.current->process->memorySpace -= 0;
	}
}