#include <stdint.h>
#include <klib.h>

void * memset(void * destination, int32_t c, uint64_t length)
{
	uint8_t chr = (uint8_t)c;
	char * dst = (char*)destination;

	while(length--)
		dst[length] = chr;

	return destination;
}

void * memcpy(void * destination, const void * source, uint64_t length)
{
	/*
	* memcpy does not support overlapping buffers, so always do it
	* forwards. (Don't change this without adjusting memmove.)
	*
	* For speedy copying, optimize the common case where both pointers
	* and the length are word-aligned, and copy word-at-a-time instead
	* of byte-at-a-time. Otherwise, copy by bytes.
	*
	* The alignment logic below should be portable. We rely on
	* the compiler to be reasonably intelligent about optimizing
	* the divides and modulos out. Fortunately, it is.
	*/
	uint64_t i;

	if ((uint64_t)destination % sizeof(uint32_t) == 0 &&
		(uint64_t)source % sizeof(uint32_t) == 0 &&
		length % sizeof(uint32_t) == 0)
	{
		uint32_t *d = (uint32_t *) destination;
		const uint32_t *s = (const uint32_t *)source;

		for (i = 0; i < length / sizeof(uint32_t); i++)
			d[i] = s[i];
	}
	else
	{
		uint8_t * d = (uint8_t*)destination;
		const uint8_t * s = (const uint8_t*)source;

		for (i = 0; i < length; i++)
			d[i] = s[i];
	}

	return destination;
}

void itoa(int num, char arr[]){
	int digits=0, aux = num;
		
	do{	
		digits++;
		aux/=10;
	} while(aux!=0);
	
	aux = digits;
	while(aux!=0){
		arr[aux-1] = num%10 + '0';
		num/= 10;
		aux--;	
	}
	arr[digits] = 0;
	return;
}

int atoi(char * arr){
	int i=0, num = 0, negative = FALSE;
	if(arr[0] == '-'){
		negative = TRUE;
		i++;
	}
	while(arr[i] != 0){
		num*=10;
		num+= arr[i];
		i++;
	}

	if(negative){
		num*= -1;
	}

	return num;
}	


void readBCD(char value, char * retArr){
	retArr[0] = value>>4;
	retArr[0] = retArr[0] & 0x0F;
	retArr[1] = value&0x0F;
	
	retArr[0]+= '0';
	retArr[1]+= '0';
	retArr[2] = 0;
}