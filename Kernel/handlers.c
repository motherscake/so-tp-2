#include <handlers.h>
#include <RTC.h>
#include <video.h>
#include <keyboard.h>
#include <process.h>
#include <virtualMemory.h>
#include <scheduler.h>
#include <interrupts.h>


int sysCallHandler(uint64_t arg1, uint64_t arg2, uint64_t arg3, uint64_t arg4){
	char time;
	Process * process;
	uint64_t * virtualPage;
	
	switch(arg4){
		
		case SYS_WRITE:
			//kPrint((char*)arg2, 1);
			return sysWrite((uintptr_t)arg1, (char*)arg2, (uintptr_t)arg3);

		case SYS_READ:
			return sysRead((uintptr_t)arg1, (char*)arg2, (uintptr_t)arg3);
		
		case SYS_SSTIME:
			kernelSetSSTime(arg1);
			break;
		
		case SYS_CLEAR:
			clearScreen();
			break;
		
		case SYS_RTC_READ:
			time = sysRTCRead(arg1);
			readBCD(time, arg2);
			break;

		case SYS_RTC_SET:
			sysRTCSet(arg1, arg2);
			break;

		case SYS_ALLOC:
			return sysAlloc((uintptr_t)arg1);
			
			//return getVirtualPage((uint64_t*)arg1);

		case SYS_FREE:
			return sysFree((uintptr_t*)arg1);
			//return freeVirtualPage((uint64_t*)arg1);

		case SYS_CREAT_PROC:
			process = newProcess((void*)arg1, (char*)arg2, (uintptr_t)arg3);
			addProcess(process);
			return process->pid;

		case SYS_DELETE_PROC:
			process = getProcessByPID((uintptr_t)arg1);
			removeProcess(process);
			break;

		case SYS_LIST_PROC:
			return listProcesses();

		case SYS_BLOCK_PROC:
			blockProcess((uintptr_t)arg1);
			break;

		case SYS_UNBLOCK_PROC:
			unblockProcess((uintptr_t)arg1);
			break;

		case SYS_DELETE_CURR_PROC:
			removeCurrentProcess();
			break;

		case SYS_BLOCK_CURR_PROC:
			blockCurrentProcess();
			break;

		case SYS_YIELD:
			_yield();
			break;

		case SYS_GET_CURRENT_PID:
			return getCurrentPid();
		
		case SYS_SET_FOREGROUND:
			setMeForegroundProcess();
			break;

		case SYS_FREE_FOREGROUND:
			freeForeground();
			break;

		case SYS_AMOUNT_PROC:
			return getProcessesAmount();
			break;

		case SYS_SHMGET:
			return shmget((uintptr_t)arg1,(uintptr_t)arg2);

		case SYS_SHMAT:
			return shmat((void*)arg1);

		case SYS_SHMDT:
			shmdt((uintptr_t)arg1);
			break;

		case SYS_SEMGET:
			return semget((uintptr_t)arg1,(uintptr_t)arg2);

		case SYS_SEMCTL:
			semctl((uintptr_t)arg1);
			break;

		case SYS_NOT_SHOW:
			notShow();
			break;

		case SYS_RESET_BUFFER:
			resetBuffer();
			break;

		case SYS_SEM_DOWN:
			down((uintptr_t)arg1);
			break;

		case SYS_SEM_UP:
			up((uintptr_t)arg1);
			break;

		case SYS_LIST_SEM_ID:
			return listsemid((uintptr_t*)arg1);

		case SYS_GET_PPID:
			return getCurrentppid();

		case SYS_LIST_SHM_ID:
			return listshmid((uintptr_t*)arg1);

		default:
			break;
	}
	return 0;
}
