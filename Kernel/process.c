#include <process.h>
#include <virtualMemory.h>

/* ADDED FOR PAGING */
#define STACK_BASE_ADDR (0x7FFFFFFF - 0x10)
//Si escribo un int en la 0x7FFFFFFF, se escribe desde el 
//0x7FFFFFFF hasta el 0x80000002 (4 bytes)  
#define HEAP_BASE_ADDR 0x40000000

static int pidsCounter = 2;

Process * newProcess(void * entryPoint, char * name, int ppid){

	uint64_t * PDT = (uint64_t *) sysAlloc(PAGE_SIZE);
	uint64_t * PT = (uint64_t *) sysAlloc(PAGE_SIZE);
	uint64_t * auxPage = (uint64_t *) sysAlloc(PAGE_SIZE);
	

	memset(PDT, 0, PageSize);
	memset(PT, 0, PageSize);
	memset(auxPage, 0, PageSize);

	PDPT[pidsCounter] = (uint64_t) PDT | 7;
	PDT[511] = (uint64_t) PT | 7;
	PT[511] = (uint64_t)auxPage | 7;
	
	Process * process = sysAlloc(sizeof(Process));
	process->pid = pidsCounter;

	process->userStackPage = ( GIGABYTE * (pidsCounter + 1) ) - 0x10;
	process->nextAlloc = ( GIGABYTE * (pidsCounter) );
	


	//Process * process = sysAlloc(sizeof(Process));
	//process->pid = pidsCounter++;
	process->name = name;
	process->memorySpace = 4; //TODO: CHANGE TO 4, add in malloc
	//process->userStackPage = sysAlloc(PAGE_SIZE);
	process->kernelStackPage = sysAlloc(PAGE_SIZE);

	pidsCounter++;
	
	setUpPaging();

	process->entryPoint = entryPoint;
	process->state = READY;

	//process->userStack = toStackAddress(process->userStackPage); Ya lo hago
	process->userStack = process->userStackPage;

	process->ppid = ppid;
	//process->userStack = toStackAddress(process->userStackPage);
	process->kernelStack = toStackAddress(process->kernelStackPage);

	process->userStack = fillStackFrame(process->entryPoint, process);
	
	return process;

}

void deleteProcess(Process * process) {
	
	//for(int i=0; i< ( (GIGABYTE * (process->pid+1) - 0x10) - process->userStackPage) / 0x1000; i++){
		//sysFree(getPhysicalPage(process->userStackPage));
	//}
	
	sysFree(process->userStackPage);
	sysFree(process->kernelStackPage);
	sysFree(process);
}

//Given a base address for a page, you add PageSize to get the upper limit 
// (The stack should grow from top to down)
void * toStackAddress(void * page) {
	return (uint8_t*)page + PAGE_SIZE - 0x10;
}

StackFrame * fillStackFrame(void * entryPoint, Process * process) {
	StackFrame * frame = (StackFrame*)(process->userStack) - 1;
	frame->gs =		0x001;
	frame->fs =		0x002;
	frame->r15 =	0x003;
	frame->r14 =	0x004;
	frame->r13 =	0x005;
	frame->r12 =	0x006;
	frame->r11 =	0x007;
	frame->r10 =	0x008;
	frame->r9 =		0x009;
	frame->r8 =		0x00A;
	frame->rsi =	0x00B;
	frame->rdi =	0x00C;
	frame->rbp =	0x00D;
	frame->rdx =	0x00E;
	frame->rcx =	0x00F;
	frame->rbx =	0x010;
	frame->rax =	0x011;
	frame->rip =	(uint64_t)entryPoint;
	frame->cs =		0x008;
	frame->eflags = 0x202;
	frame->rsp =	(uint64_t)&(frame->base);
	frame->ss = 	0x000;
	frame->base =	0x000;
	return frame;
}



