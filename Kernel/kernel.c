#include <stdint.h>
#include <klib.h>
#include <moduleLoader.h>
#include <interrupts.h>
#include <attributes.h>
#include <handlers.h>
#include <allocator.h>
#include <scheduler.h>

/* ADDED FOR PAGING */
#include <virtualMemory.h>

IDTEntry * idt = (IDTEntry*) 0x0;

typedef int (*EntryPoint)();

extern uint8_t text;
extern uint8_t rodata;
extern uint8_t data;
extern uint8_t bss;
extern uint8_t endOfKernelBinary;
extern uint8_t endOfKernel;

//static const uint64_t PageSize = 0x1000;

static void * const sampleCodeModuleAddress = (void*)0x400000;
static void * const sampleDataModuleAddress = (void*)0x500000;

void setupIDTEntry (int index, uint16_t selector, uint64_t offset, uint8_t type_attr);

void initializeIDT();

void nullTask(void);

void clearBSS(void * bssAddress, uint64_t bssSize)
{
	memset(bssAddress, 0, bssSize);
}

void * getStackBase()
{
	return (void*)(
		(uint64_t)&endOfKernel
		+ PageSize * 8				//The size of the stack itself, 32KiB
		- sizeof(uint64_t)			//Begin at the top of the stack
	);
}

void * initializeKernelBinary()
{
	void * moduleAddresses[] = {sampleCodeModuleAddress,sampleDataModuleAddress};

	loadModules(&endOfKernelBinary, moduleAddresses);

	clearBSS(&bss, &endOfKernel - &bss);

	return getStackBase();
}

void initializeIDT()
{	
	setupIDTEntry(0x20, 0x08, (uint64_t)&_irq00Handler, ACS_INT);
	setupIDTEntry(0x21, 0x08, (uint64_t)&_irq01Handler, ACS_INT);
	setupIDTEntry(0x80, 0x08,(uint64_t)&_int80Handler, ACS_INT);

	setupIDTEntry(0x0E, 0x08,(uint64_t)&_pageFaultHandler, ACS_INT);
	
	return;
}

int main(){
	
	clearScreen();

	initializeIDT();

	initScheduler();

	initializeAllocator();

	picMasterMask(0xFC);

	picSlaveMask(0xFF);

	/* ADDED FOR PAGING */
	
	initializePaging();

	Process * process = newProcess((void *)sampleCodeModuleAddress, "shell", 0);

	//Main Process (Userland)
	//Process * process = newProcess((void *)sampleCodeModuleAddress);
	
	addProcess(process);

	//NullTask Process
	//Process * process2 = newProcess((void *)nullTask);
	Process * process2 = newProcess((void *)nullTask, "nullTask", 0);

	addProcess(process2);
	
	_sti();

	((EntryPoint)sampleCodeModuleAddress)();
	
	return 0;
}

void nullTask(void){
	while(1){}
}



void setupIDTEntry (int index, uint16_t selector, uint64_t offset, uint8_t type_attr) {
   idt[index].offsetLow = offset & 0xFFFF;
   offset>>=16;
   idt[index].selector = selector;
   idt[index].zero1 = 0;
   idt[index].type_attr = type_attr;
   idt[index].offsetMid =offset & 0xFFFF;
   offset>>=16;
   idt[index].offsetHigh = offset;
   idt[index].zero2 = 0;
   idt[index].zero3 = 0;
   idt[index].zero4 = 0;
   idt[index].zero5 = 0;  
}

